#' Load data, e.g. S&P500 time series
#'
#' 
#' @param file_in name of file to load data from, default from constatns
#' @param start_date beginning of the data
#' @param end_date ending of the data
#' @param date_volume_only logical, load only date and volume
#' @return data.frame
read_fin_data <- function(file_in = DATA_IN_SP,
                          start_date = NULL,
                          end_date = NULL,
                          date_volume_only = TRUE){
    
    # read from csv
    data <- read.csv2(file_in, header=T, sep=",", dec=".", stringsAsFactors=F)
    
    names(data) <- tolower(names(data)) # column names in lowercase
    data$date <- as.Date(data$date)     # convert to Date format
    
    # extract only date and volume if necessary
    if(date_volume_only){
        data <- data[c("date", "volume")]
    }
    
    # filter from starting date
    if(!is.null(start_date)){
        # start_date <- "2013-02-04"
        data <- data[data$date >= as.Date(start_date),]
    }
    
    # filter until ending date
    if(!is.null(end_date)){
        # end_date <- "2013-08-04"
        data <- data[data$date <= as.Date(end_date),]
    }
    
    return(data)
}

#' Read data in format downloaded from investing.com
#'
#' 
#' @param file_in name of file to load data from, default from constatns
#' @param date_volume_only logical, load only date and volume
#' @return data.frame
read_investing_data <- function(file_in = NULL, date_volume_only = FALSE){
    
    # read from csv
    data <- read.csv2(file_in, header=T, sep=",", dec=".", stringsAsFactors=F)
    
    names(data) <- tolower(names(data)) # column names in lowercase
    
    # convert column names to the common format
    names(data)[grepl(pattern="date", names(data))] <- "date"
    names(data)[grepl(pattern="vol", names(data))] <- "volume"
    names(data)[grepl(pattern="change", names(data))] <- "change"
    
    # convert volume from character to numeric
    if("volume" %in% names(data)){
        a <- data$volume
        a[grepl("K", a)] <- as.numeric(gsub("K", "", a[grepl("K", a)])) * 10^3
        a[grepl("M", a)] <- as.numeric(gsub("M", "", a[grepl("M", a)])) * 10^6
        a[grepl("B", a)] <- as.numeric(gsub("B", "", a[grepl("B", a)])) * 10^9
        data$volume <- suppressWarnings(as.numeric(a))
    }
    
    # convert price, open, high, low to numeric
    col_to_convert <- c("price", "open", "high", "low")
    for(col in col_to_convert){
        if(col %in% names(data)){
            data[col] <- as.numeric(gsub(",", "", data[,col]))
        }
    }
    
    # convert date
    data$date <- readr::parse_date(data$date, "%b %d, %Y", locale=locale("en"))
    
    # order ascending by date
    data <- data[order(data$date),]
    
    # extract only date and volume if necessary
    if(date_volume_only){
        data <- data[c("date", "volume")]
    }
    
    return(data)
}

#' Read economic calendar data
#'
#' Read economic calendar data from csv stored in format as exported from 
#' https://www.fxstreet.com/economic-calendar.
#' User can choose either to read a specific file or a whole folder.
#' Note that warnings are suppressed when reading the data 
#' 
#' @param file_in csv or folder name to read from
#' @param filter_country: string with country name to filter (keep, e.g. "United States")
#' @param high_volatility_only: logical, keep only records with the highest expected volatility?
#' @param group_by_date: logical, use maximum volatility for multiple records per day
#' @param discard_na_volatility: logical, discard observations with NA expected volatility
#' @return data.frame
read_economic_calendar_data <- function(file_in = NULL,
                                        filter_country = NULL,
                                        high_volatility_only = FALSE,
                                        group_by_date = TRUE,
                                        discard_na_volatility = TRUE){
    
    # debug purposes
    # file_in <- "d:/codes/sp_volume/data/in/economic_calendar_data/eventdates.csv"
    # file_in <- "d:/codes/sp_volume/data/in/economic_calendar_data"
    
    # use default folder if file_in not supplied
    if(is.null(file_in)){
        file_in <- DATA_IN_ECONOMIC_CALENDAR_FLD
    }
    
    # is file_in a csv file or a folder?
    if(grepl(".csv$", file_in)){
        # case csv file
        data <- suppressWarnings(read.table(file = file_in, header = T, 
                                            sep=",", dec=".", stringsAsFactors = F))
    }else{
        # case folder
        data <- data.frame() # declare resulting df
        
        # iterate over each data file, read and rbind to resulting df
        for(i in 1:length(dir(file_in))){
            file_now <- file.path(file_in, dir(file_in)[i])
            data_now <- suppressWarnings(read.table(file = file_now, header = T, 
                                                    sep=",", dec=".", stringsAsFactors = F))
            data <- rbind(data, data_now)
        }
    }
    
    names(data) <- tolower(names(data))  # lowercase column names
    
    # parse datetime to create date and time columns
    data$date <- readr::parse_date(data$datetime, "%m/%d/%Y %H:%M:%S", locale=locale("en"))
    data$time <- readr::parse_time(data$datetime, "%m/%d/%Y %H:%M:%S", locale=locale("en"))
    
    # drop NA expected volatility
    if(discard_na_volatility){
        # discard data with NA expected volatiliy
        drop_idx_na <- (is.na(data$volatility))
        data <- data[!drop_idx_na, ]
        cat("read_economic_calendar_data: Discarded", sum(drop_idx_na), 
            "observations from economic calendar data. (NA expected volatility filter)\n")
    }
    
    # filter by expected volatility
    if(high_volatility_only){
        drop_idx_volatility <- (data$volatility < 3) # row indices to discard
        data <- data[!drop_idx_volatility, ]
        cat("read_economic_calendar_data: Discarded", sum(drop_idx_volatility), 
            "observations from economic calendar data. (high volatility filter)\n")
    }
    
    # filter by country
    if(!is.null(filter_country)){
        drop_idx_country <- (data$country != filter_country) # row indices to discard
        data <- data[!drop_idx_country, ]
        cat("read_economic_calendar_data: Discarded", sum(drop_idx_country), 
            "observations from economic calendar data. (country filter)\n")
    }
    
    # group by date
    if(group_by_date){
        data <- data %>%
            dplyr::select(date, volatility) %>%
            dplyr::group_by(date) %>%
            dplyr::summarise(volatility = max(volatility))
    }
    
    return(as.data.frame(data))    
}

#' Reads investing data and calculates pct change in price
#'
#' 
#' @param file_in string of file names
#' @param start_date beginning of the data
#' @param end_date ending of the data
#' @param pct_change logical, express price as percentage change
#' @return data.frame
read_preprocess_investing_data <- function(file_in = c("NI225.csv", "USDCHF.csv", "UST10Y.csv", "UST3M.csv", "GSPC.csv"),
                                           start_date = NULL,
                                           end_date = NULL,
                                           pct_change = TRUE){

    files <- file_in
    
    # get just dates from one of the files 
    data_complete <- read_investing_data(file_in = file.path(DATA_IN_OTHER_ECONOMIC_FLD, files[1]))["date"]
    
    # iterate over supplid files
    for(file in files){
        
        # read data
        data_now <- read_investing_data(file_in = file.path(DATA_IN_OTHER_ECONOMIC_FLD, file))
        
        # should data be expressed as percentage change
        if(pct_change){
            # pct change
            data_now <- data_now %>% 
                mutate(price_change = (price/lead(price) - 1) * 100) %>%
                select(date, price_change)
        }
        
        # put here name 
        names(data_now)[names(data_now) == "price_change"] <- gsub(".csv", "", file)
        
        # merge with complete data
        data_complete <- merge(data_complete, data_now, all.x = T)
    }
    
    # only complete cases and input Infinity
    data_complete <- data_complete[complete.cases(data_complete), ]
    data_complete[data_complete == Inf] <- 0
    
    # filter from starting date
    if(!is.null(start_date)){
        # start_date <- "2013-02-04"
        data_complete <- data_complete[data_complete$date >= as.Date(start_date),]
    }
    
    # filter until ending date
    if(!is.null(end_date)){
        # end_date <- "2013-08-04"
        data_complete <- data_complete[data_complete$date <= as.Date(end_date),]
    }
    
    return(data_complete)
}


#' Reads sentiment data
#'
#' 
#' @param derived_twitter_sentiment_fld folder with derived sentiment
#' @return data.frame
read_sentiment_data <- function(derived_twitter_sentiment_fld = DERIVED_TWITTER_SENTIMENT_FLD){
    
    # declare empty df
    data_full <- data.frame()
    
    # iterate over each file in source folder
    for(file in dir(derived_twitter_sentiment_fld)){
        
        # read and rbind to results    
        data_now <- read_csv(file.path(derived_twitter_sentiment_fld, file), col_types = cols())
        data_full <- rbind(data_full, data_now)
    }
    
    # aggregate to single observation per date
    data_agg <- data_full %>%
        group_by(date) %>%
        mutate(polarity = mean(polarity), subjectivity = mean(subjectivity))
    
    return(data_agg)
    
}