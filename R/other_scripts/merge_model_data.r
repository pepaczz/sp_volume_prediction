#' Read predictions of supplied models data
#'
#' 
#' @param list_of_models list of strings with model names (i.e. folder names, e.g. taken from constants)
#' @param return_gg_format logical, return long format (T) or wide format (F, default)
#' @param filter_year integer, single year to be filtered for
#' @param start_date beginning of the data
#' @param end_date ending of the data
#' @return data.frame
merge_model_data <- function(list_of_models,
                             return_gg_format = F,
                             filter_year = NULL,
                             start_date = TEST_START,
                             end_date = TEST_END){

    # get SP500 data
    data_full <- read_fin_data()
    names(data_full) <- c("date", "observed")
    
    # filter selected year
    if(!is.null(filter_year)){
        # filter_year <- 2012
        data_full <- data_full[(year(data_full$date) %in% filter_year),]
    }
    
    # filter from starting date
    if(!is.null(start_date)){
        # start_date <- "2013-02-04"
        start_date <- as.Date(start_date)
        data_full <- data_full[data_full$date >= start_date,]
    }
    
    # filter until ending date
    if(!is.null(end_date)){
        # end_date <- "2013-08-04"
        end_date <- as.Date(end_date)
        data_full <- data_full[data_full$date <= end_date,]
    }
    
    # iterate over all required models to merge data
    for(model in list_of_models){
        
        # model <- list_of_models[1] # debug purposes
        
        # read data from model and rename
        model_data_now <- read_fin_data(file.path(file.path(DATA_MODELS_OUT_FLD, model, "prediction.csv")))
        model_data_now <- model_data_now[c("date", "volume")]
        names(model_data_now) <- c("date", model)
        
        # merge data together
        data_full <- merge(x = data_full, y = model_data_now, by = "date", all.x = T)
    }
    
    # remove incomplete rows
    n_incomplete <- sum(!complete.cases(data_full))
    if(n_incomplete > 0){
        cat("merge_model_data: removed", n_incomplete, "rows from merged data! \n")
        data_full <- data_full[complete.cases(data_full),]
    }
    
    # melt to gg format if required
    if(return_gg_format){
        result <- melt(data_full, id.vars = "date", variable.name = "model")
    }else{
        result <- data_full
    }
    
    return(result)
}


