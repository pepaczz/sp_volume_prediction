#' Calculates RMSE
#'
#' 
#' @param x numeric vector
#' @param y numeric vector
#' @return numeric
get_rmse <- function(x,y){
    return(( sum((x - y)^2, na.rm = T) / length(x) )^(1/2))
}

#' Calculates SSE and RMSE for supplied models
#'
#' 
#' @param list_of_models list of strings with model names (i.e. folder names, e.g. taken from constants)
#' @param start_date beginning of the data
#' @param end_date ending of the data
#' @return data.frame
get_evaluation_metrics <- function(list_of_models,
                                   start_date = TEST_START,
                                   end_date = TEST_END){

    # load predictions
    data <- merge_model_data(list_of_models, return_gg_format = F,
                             start_date = start_date, end_date = end_date)
    
    # create empty df
    results_df <- data.frame(model = character(), sse = double())
    
    # iterate over each supplied model
    for(model in list_of_models){
        # model <- list_of_models[1] # debug purposes
        
        # calculate sse
        sse = sum((data$observed - data[model][,1])^2, na.rm = T)
        rmse = get_rmse(data$observed, data[model][,1])
        
        # bind to results
        df_now <- data.frame(model = model, sse = sse, rmse = rmse)
        results_df <- rbind(results_df, df_now)
    }
    
    return(results_df)
    
}