#' Generate plot of S&P500 volume time series and save to file
#'
#' 
#' @param start_date beginning of the data
#' @param end_date ending of the data
#' @param fig_file name of file (without path). Default "sp_time_series.png". If NULL figure is not saved
#' @return ggplot figure object
plot_sp_series <- function(start_date = "2009-01-01",
                           end_date = TEST_END,
                           fig_file = "sp_time_series.png"){
    
    # load SP data
    data_sp <- read_fin_data(start_date = "2009-01-01", end_date = TEST_END)
    
    # generate figure
    fig_sp <- ggplot(data_sp, aes(x=date, y=volume)) + geom_line() +
        geom_ma(ma_fun = SMA, n = 200) + ylab("volume")
    
    # save figure
    if(!is.null(fig_file)){
        cat("plot_sp_series: figure generated \n")
        ggsave(filename = file.path(FIGURES_FLD, fig_file),
               plot = fig_sp,width = 9, height = 3.5, units = "in")
    }
    
    return(fig_sp)
}

#' Generate plot of S&P500 volume time series with various EMA
#'
#' 
#' @param start_date beginning of the data
#' @param end_date ending of the data
#' @param fig_file name of file (without path). Default "sp_ema.png". If NULL figure is not saved
#' @return ggplot figure object
plot_sp_ema <- function(start_date = as.Date("2011-01-01"),
                        end_date = as.Date("2011-04-01"),
                        fig_file = "sp_ema.png"){
    
    # get SP500 data
    sp_data <- read_fin_data(start_date = TRAIN_START, end_date = TEST_END)
    
    # get EMA series and rename them
    sp_ema_5 <- get_sp_ema_series(ema_order = 5)
    names(sp_ema_5) <- c("date", "volume_ema5")
    
    sp_ema_10 <- get_sp_ema_series(ema_order = 10)
    names(sp_ema_10) <- c("date", "volume_ema10")
    
    sp_ema_30 <- get_sp_ema_series(ema_order = 30)
    names(sp_ema_30) <- c("date", "volume_ema30")
    
    # merge data together
    data_full <- merge(sp_data, sp_ema_5, all.x = T)
    data_full <- merge(data_full, sp_ema_10, all.x = T)
    data_full <- merge(data_full, sp_ema_30, all.x = T)
    
    # melt data for gg format
    data_melt <- melt(data_full, id.vars = "date") %>%
        filter(date >= start_date, date <= end_date)
    
    # generate plot
    fig_sp_ema <- ggplot(data_melt, aes(x=date, y=value, colour=variable)) + 
        geom_line() +
        ylab("volume") +
        theme(legend.position="bottom")
    
    # save the plot
    if(!is.null(fig_file)){
        cat("plot_sp_ema: figure generated \n")
        ggsave(filename = file.path(FIGURES_FLD, fig_file),
               plot = fig_sp_ema,width = 9, height = 3.5, units = "in")
    }
    return(fig_sp_ema)
}


#' Generates qqplots for supplied models
#'
#' 
#' @param list_of_models list of strings with model names (i.e. folder names, e.g. taken from constants)
#' @param start_date beginning of the data
#' @param end_date ending of the data
#' @param models_per_group how many models in one plot. Creates facetes
#' @param fig_file name of file (without path). Default "residuals_densities.png". If NULL figure is not saved
#' @return ggplot figure object
plot_residuals_densities <- function(list_of_models,
                                     start_date = TEST_START,
                                     end_date = TEST_END,
                                     models_per_group = 3,
                                     fig_file = "residuals_densities.png"){
    
    # get estimates of models
    data <- merge_model_data(list_of_models, return_gg_format = F,
                             start_date = start_date, end_date = end_date)
    
    # model <- list_of_models[1] # debug purposes only
    
    # declare empty dataframe
    df_residuals <- data.frame()
    
    i <- 0 # counter
    # iterate over each supplied model
    for(model in list_of_models){
        
        i <- i+1 # increase counter
        
        # calculate residuals
        df_residuals_now <- data.frame(date = data$date, 
                                       error = data$observed - data[model][,1],
                                       model = model,
                                       model_group = floor(((i-1) / models_per_group) + 1))
        
        # rind to results
        df_residuals <- rbind(df_residuals, df_residuals_now)
    }
    
    # generate plot - density plots
    # fig_resid <- ggplot(df_residuals, aes(x = error, colour = model)) +
    #     geom_density() +
    #     facet_grid(. ~ model_group)+
    #     theme(legend.position="bottom") +
    #     theme(strip.text.x = element_blank())
    
    # generate plot - qqplot
    fig_resid <- ggplot(df_residuals, aes(sample = error, colour = model)) +
        stat_qq() +
        stat_qq_line() +
        facet_grid(. ~ model_group)+
        theme(legend.position="bottom") +
        theme(strip.text.x = element_blank())
    
    # save plot if required
    if(!is.null(fig_file)){
        cat("fig_resid: figure generated \n")
        ggsave(filename = file.path(FIGURES_FLD, fig_file),
               plot = fig_resid,width = 9, height = 4, units = "in")
    }
    
    return(fig_resid)
}
