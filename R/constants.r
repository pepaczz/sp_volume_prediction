R_CODES_FLD <- "R"

# main data folders
DATA_IN_FLD <- "data/in"
DATA_MODELS_OUT_FLD <- "data/models_prediction"

### input data

# s&p data file
DATA_IN_SP <- file.path(DATA_IN_FLD, "sp500_data/GSPC.csv")

# other data sources
DATA_IN_OTHER_ECONOMIC_FLD <-  file.path(DATA_IN_FLD, "other_economical_data")
DATA_IN_ECONOMIC_CALENDAR_FLD <-  file.path(DATA_IN_FLD, "economic_calendar_data")

### figures
FIGURES_FLD <- "figures"

### output data

# model files folder
MODEL_FILES_FLD <- "data/model_files_r"

# model names
# MODEL_ARIMA <- "model_arima"
# MODEL_ARIMA_XREG <- "model_arima_xreg"

MODEL_REF <- "model_ref"
MODEL_CALENDAR_HV <- "model_calendar_hv"
MODEL_CALENDAR_LV <- "model_calendar_lv"
MODEL_NIKKEI <- "model_nikkei"
MODEL_NIKKEI_SQ <- "model_nikkei_sq"
MODEL_SENTIMENT <- "model_sentiment"
MODEL_SENTIMENT_SQ <- "model_sentiment_sq"
MODEL_EMA <- "model_ema"
MODEL_OTHER_ECONOMIC <- "model_other_economic"
MODEL_OTHER_ECONOMIC_SQ <- "model_other_economic_sq"

# other data outputs
DERIVED_TWITTER_SENTIMENT_FLD <- "data/sentiment_data/derived_sentiment"

# ema calibratin
CALIBRATION_EMA_FLD <- "data/ema_calibration"

# dates
TRAIN_START <- as.Date("2010-01-01")
TRAIN_END <- as.Date("2014-07-31")
TEST_START <- as.Date("2014-08-01")
TEST_END <- as.Date("2016-12-31")

### DEPRECATED
# data outputs of models
MODEL_REF_DATA_FLD <- file.path(DATA_MODELS_OUT_FLD, "model_ref")
MODEL_ARIMA_DATA_FLD <- file.path(DATA_MODELS_OUT_FLD, "model_arima")
MODEL_ARIMA_DIFF_DATA_FLD <- file.path(DATA_MODELS_OUT_FLD, "model_arima_diff")
MODEL_CALENDAR_MA_DEVIATION_DATA_FLD <- file.path(DATA_MODELS_OUT_FLD, "model_calendar_ma_deviation")

