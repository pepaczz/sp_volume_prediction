#' Predicts reference model
#'
#' 
#' @param start_date beginning of the data
#' @param end_date ending of the data
#' @param prediction_file prediction file name, defaults to model name
#' @param write_to_file save prediction
#' @return model object or NULL
model_ref <- function(start_date,
                      end_date,
                      prediction_file = NULL,
                      write_to_file = T){
    
    # read data
    data <- read_fin_data()

    data <- data %>%
        mutate(volume = lag(volume)) %>%
        filter(date >= start_date, date <= end_date)
 
    # export to csv
    if(write_to_file){
        
        # output file address
        if(is.null(prediction_file)){
            prediction_file <- file.path(DATA_MODELS_OUT_FLD, MODEL_REF, "prediction.csv")
        }
        
        write.table(data, file = prediction_file, sep = ",", row.names = F)
        cat("model_ref: model written to", prediction_file, "\n")
        
    }
    # return(prediction)
    return(NULL)
}


