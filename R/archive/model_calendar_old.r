source("R/codes/constants.r")

require(forecast)
require(reshape2)
require(lubridate)
require(dplyr)
require(ggplot2)
require(dplyr)
require(HDCI)

model_calendar_ma_deviation <- function(sp_file_in = DATA_IN_SP, 
                                        economic_calendar_folder_file_in = NULL,
                                        ma_order = 10,
                                        lags = c(-2,-1,1,2),
                                        fit_method = "ols",
                                        file_out = file.path(MODEL_CALENDAR_MA_DEVIATION_DATA_FLD, "out.csv"),
                                        write_to_file = T){
    # 
    #
    # args:
    #   sp_file_in: name of input file, default defined in constants.r
    #   economic_calendar_folder_file_in: 
    #   file_out: name of output file, default defined in constants.r
    #   write_to_file: decide whether to write into file or not, default TRUE
    #
    # returns:
    #   data.frame with date and prediction
    
    # load SP data and use only complete cases
    sp_data <- read_fin_data(file_in = sp_file_in)
    
    # calculate moving average
    sp_data["volume_ma"] <- ma(x = sp_data$volume, ma_order)
    
    # get deviation from trend
    sp_deviation <- sp_data %>%
        dplyr::mutate(volume_dev = volume - volume_ma) %>%
        dplyr::select(date, volume_dev) %>%
        as.data.frame()
    sp_deviation <- sp_deviation[complete.cases(sp_deviation),]
    
    # read calendar data
    calendar_data <- read_economic_calendar_data(file_in = economic_calendar_folder_file_in,
                                                 filter_country = "United States", 
                                                 high_volatility_only = T, 
                                                 group_by_date = T)
    
    # join data and prepare them for modeling
    model_data <- merge(sp_deviation, calendar_data, all.x = T)
    
    # generate leads and lags
    model_data <- generate_lead_lags(model_data, lags)
    
    # fit models
    if(fit_method == "ols"){
        
        # input data for the model
        ols_data <- model_data[ , !(names(model_data) %in% c("date", "volatility"))]
        
        # fit and save predictions
        fit_ols <- lm(volume_dev ~ ., ols_data)
        predictions <- fit_ols$fitted.values 
        
    }else if(fit_method == "lasso"){
        
        # input data into model
        x <- model_data[ , !(names(model_data) %in% c("volume_dev", "date", "volatility"))]
        y <- model_data$volume_dev
        
        # fit and save predictions
        fit_lasso <- HDCI::Lasso(x=as.matrix(x), y=y, fix.lambda = F)
        # summary(fit_lasso)
        predictions <- mypredict(fit_lasso, newx = as.matrix(x))
    }
    
    # generate result
    result_w_dates <- cbind(pred = predictions, model_data)[,c("pred","date")]
    result_w_ma <- merge(x = result_w_dates, y=sp_data, all.x=T)
    result <- result_w_ma %>%
        dplyr::mutate(volume = pred + volume_ma) %>%
        dplyr::select(date, volume)
    
    # export to csv
    if(write_to_file){
        write.table(result, file = file_out, sep = ",", row.names = F)
    }
    
    return(result)
}

generate_lead_lags <- function(data, lags, drop_incompete=T){
    # Generates event columns for each volatility level and its leads and lags
    #
    # args:
    #   data: dataframe with volatility column
    #   lags: vector of integers specifying lags. If negative, generates leads
    #
    # returns: dataframe with new columns
    #  
    
    # get levels of volatility
    volatility_levels <- sort(unique(data$volatility))
    
    for(volatility_level in volatility_levels){
        var_name_base <- paste0("event_lvl", volatility_level, "_")
        
        # without_lag
        var_name_t0 <- paste0(var_name_base, "t0")
        data[var_name_t0] <- ifelse(is.na(data$volatility), 0, 
                                    ifelse(data$volatility == volatility_level, 1, 0))
        
        # data <- as.ts(data)
        # iterate over each lag
        for(lag_now in lags){
            
            # lagged variable name
            var_name_now <- paste0(var_name_base, "t", lag_now)
            
            # generate lag or lead
            if(lag_now > 0){
                data[var_name_now] <- dplyr::lag(data[,var_name_t0], lag_now)    
            }else{
                data[var_name_now] <- dplyr::lead(data[,var_name_t0], -lag_now)
            }
        }
        
    }
    
    # drop incompete rows
    if(drop_incompete){
        if(max(lags) > 0){ # lags
            data <- data[(max(lags)+1):nrow(data),]
        }
        if(min(lags) < 0){ # leads
            data <- data[0:(nrow(data)+min(lags)),]
        }
        
        cat("generate_lead_lags: Discarded", max(c(lags, 0)), "/", -min(c(lags, 0)),
            "observations from data lags / leads)\n")
    }
    
    return(data)
}