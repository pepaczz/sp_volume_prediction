source("R/codes/constants.r")

require(forecast)

model_arima <- function(file_in = DATA_IN_SP,
                        start_date = TRAIN_START,
                        end_date = TRAIN_END,
                        file_out = file.path(MODEL_FILES_FLD, paste0(MODEL_ARIMA, ".rds")),
                        write_to_file = TRUE){
    # Arima model without differencing (stationary time series)
    #
    # args:
    #   file_in: name of input file, default defined in constants.r
    #   start_date: starting date for data used for modelling
    #   start_date: starting date for data used for modelling
    #   file_out: name of output file, default defined in constants.r
    #   write_to_file: decide whether to write into file or not, default TRUE
    #
    # returns:
    #   data.frame with date and prediction
    
    # read data
    data <- read_fin_data(file_in = file_in, start_date = start_date, end_date = end_date)
    # data <- data[1:30, ] # debug purposes - only few rows
    
    data <- data[c("date", "volume")]   # select only date and volume
    
    # fit ARIMA model
    fit <- auto.arima(data$volume)
    
    # export to csv
    if(write_to_file){
        saveRDS(fit, file=file_out)
        cat("model_arima: model written to", file_out, "\n")
    }
    
    return(fit)
}

model_arima_xreg <- function(file_in = DATA_IN_SP,
                             start_date = TRAIN_START,
                             end_date = TRAIN_END,
                             model_file = NULL,
                             prediction_file = NULL,
                             write_to_file = T,
                             fit_model = TRUE){
    # Arima model with regressors
    #
    # args:
    #   file_in: name of input file for main time series
    #   start_date: starting date for data
    #   start_date: starting date for data
    #   model_file: address to the model file, defaults to data/model_files_r/model_arima_xreg.rds
    #   prediction_file: address to the model file, defaults to 
    #                               data/models_prediction/model_arima_xreg/prediction.csv
    #   write_to_file: logical, write model or prediction to file
    #   fit_model: logical, fit (TRUE) or predict (FALSE)
    #
    # returns:
    #   data.frame with date and prediction
    
    # read sp data
    data_sp <- read_fin_data(file_in = file_in, start_date = start_date, end_date = end_date, date_volume_only = T)
    # data <- data[1:30, ] # debug purposes - only few rows
    
    data_sp <- data_sp[c("date", "volume")]   # select only date and volume
    
    # investing data
    oe_data <- read_preprocess_investing_data(start_date = start_date, end_date = end_date)
    
    # lag variable
    oe_data["USDCHF_l1"] <- dplyr::lag(oe_data$USDCHF)
    oe_data["UST10Y_l1"] <- dplyr::lag(oe_data$UST10Y)
    oe_data["UST3M_l1"] <- dplyr::lag(oe_data$UST3M)
    
    # merge and prepare
    data <- merge(data_sp, oe_data, all.x = T)
    data <- data[complete.cases(data), ]
    xreg <- data %>% select(-date, -volume, -USDCHF, -UST10Y, -UST3M)
    
    # fit the model ##########################
    if(fit_model){
        
        # fit ARIMA model
        fit <- auto.arima(data$volume, xreg = xreg)
        fit_2 <- arima(x = data$volume, xreg = xreg, order = c(fit$arma[1],0,fit$arma[2]), include.mean = F)
        
        summary(fit)
        summary(fit_2)
        
        predict(fit_2, )
        
        # export to file
        if(write_to_file){
            
            # output file address
            if(is.null(model_file)){
                model_file <- file.path(MODEL_FILES_FLD, paste0(MODEL_ARIMA_XREG, ".rds"))
            }
            
            # save to file
            saveRDS(fit, file=model_file)
            cat("model_arima_xreg: model written to", model_file, "\n")
        }
        
        return(fit)
        
        # predict model ##########################  
    }else{
        
        # model file address
        if(is.null(model_file)){
            model_file <- file.path(MODEL_FILES_FLD, paste0(MODEL_ARIMA_XREG, ".rds"))
        }
        
        # read model file
        fit <- readRDS(model_file)
        
        # make prediction
        prediction <- predict(fit, newxreg = xreg)$pred
        prediction_df <- data.frame(date = data$date, volume = prediction)
        
        # export to csv
        if(write_to_file){
            
            # output file address
            if(is.null(prediction_file)){
                prediction_file <- file.path(DATA_MODELS_OUT_FLD, MODEL_ARIMA_XREG, "prediction.csv")
            }
            
            write.table(prediction_df, file = prediction_file, sep = ",", row.names = F)
            cat("model_arima_xreg: model written to", prediction_file, "\n")
            
        }
        return(prediction_df)
    }
}

