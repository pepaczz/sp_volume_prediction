source("R/codes/constants.r")
source(file.path(R_CODES_FLD, "model_ref.r"))
source(file.path(R_CODES_FLD, "model_arima.r"))
source(file.path(R_CODES_FLD, "model_calendar.r"))
source(file.path(R_CODES_FLD, "read_data.r"))
source(file.path(R_CODES_FLD, "plot_functions.r"))
source(file.path(R_CODES_FLD, "data_manipulation_functions.r"))
source(file.path(R_CODES_FLD, "evaluation_functions.r"))

require(ggplot2)
require(dplyr)


fit <- model_arima_xreg(write_to_file = T)
prediction <- model_arima_xreg(write_to_file = T, start_date = TEST_START, end_date = TEST_END, fit_model = FALSE)

read_investing_data(file_in = "data/in/other_economical_data/USDCHF.csv", )



file_in = "data/in/other_economical_data/USDCHF.csv"
file_in = "data/in/other_economical_data/UST10Y.csv"

xx <- read_preprocess_investing_data(start_date = TRAIN_START, end_date = TRAIN_END)
