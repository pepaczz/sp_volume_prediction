require(readr)
require(dplyr)

#' Read csv with derived sentiment polarities and subjectivities
#'
#' 
#' @param derived_twitter_sentiment_fld folder with derived twitter sentiment
#' @return dataframe
read_sentiment_data <- function(derived_twitter_sentiment_fld = DERIVED_TWITTER_SENTIMENT_FLD){
    
    # declare empty df
    data_full <- data.frame()
    
    # iterate over each file in source folder
    for(file in dir(derived_twitter_sentiment_fld)){
        
        # read and rbind to results    
        data_now <- read_csv(file.path(derived_twitter_sentiment_fld, file), col_types = cols())
        data_full <- rbind(data_full, data_now)
    }
    
    # aggregate to single observation per date
    data_agg <- data_full %>%
        group_by(date) %>%
        mutate(polarity = mean(polarity), subjectivity = mean(subjectivity))
    
    return(data_agg)
    
}