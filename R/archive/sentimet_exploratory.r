source("R/codes/constants.r")

require(readr)
require(reshape2)
require(ggplot2)

name <- 'sentiment_got_economics_1H2017.csv'

# iterate over each 
sentiment <- read_csv(file.path(DATA_TWITTER_SENTIMENT, name), col_types = cols())
sentiment$date <- sentiment$date + 1

# read sp volume data
sp_volume <- read_fin_data()

joint_data <- merge(sentiment, sp_volume, all.x = T)
joint_data <- joint_data[order(joint_data$date), ]
joint_data['index'] <- 1:nrow(joint_data)

# differencing
# with incomplete cases
joint_data$subjectivity_d1a <- c(NA, diff(joint_data$subjectivity))
joint_data$polarity_d1a <- c(NA, diff(joint_data$polarity))

joint_data_complete <- joint_data[complete.cases(joint_data), ]

# complete cases
joint_data_complete$subjectivity_d1b <- c(NA, diff(joint_data_complete$subjectivity))
joint_data_complete$polarity_d1b <- c(NA, diff(joint_data_complete$polarity))
joint_data_complete$volume_d1b <- c(NA, diff(joint_data_complete$volume))

joint_data_complete$subjectivity_ld1b <- c(NA, diff(log(joint_data_complete$subjectivity)))
joint_data_complete$polarity_ld1b <- c(NA, diff(log(joint_data_complete$polarity + 1)))
joint_data_complete$volume_ld1b <- c(NA, diff(log(joint_data_complete$volume)))


z <- c(1,6,8,5,2,5,9,1)
diff(z)




# line plot polarity vs volume
gg_data_1 <- melt(joint_data[c("index", "date", "polarity", "subjectivity", "volume")], 
                  id.vars = c("date", "index"))

gg_data_2 <- gg_data_1[complete.cases(gg_data_1),]

ggplot(gg_data_2, aes(x = index, y = value, colour = variable)) +
    geom_line()



###




###

ggplot(joint_data, aes(x = polarity, y = volume)) +
    geom_point()
ggplot(joint_data, aes(y = polarity, x = volume)) +
    geom_point()

ggplot(joint_data, aes(x = subjectivity, y = volume)) +
    geom_point()
ggplot(joint_data, aes(y = subjectivity, x = volume)) +
    geom_point()

# _d1a

ggplot(joint_data_complete, aes(x = polarity_d1a, y = volume)) +
    geom_point()
ggplot(joint_data_complete, aes(y = polarity_d1a, x = volume)) +
    geom_point()

ggplot(joint_data_complete, aes(x = subjectivity_d1a, y = volume)) +
    geom_point()
ggplot(joint_data_complete, aes(y = subjectivity_d1a, x = volume)) +
    geom_point()

# _d1b

ggplot(joint_data_complete, aes(x = polarity_d1b, y = volume)) +
    geom_point()
ggplot(joint_data_complete, aes(y = polarity_d1b, x = volume)) +
    geom_point()

ggplot(joint_data_complete, aes(x = subjectivity_d1b, y = volume)) +
    geom_point()
ggplot(joint_data_complete, aes(y = subjectivity_d1b, x = volume)) +
    geom_point()

ggplot(joint_data_complete, aes(x = subjectivity_d1b, y = volume_ld1b)) +
    geom_point()
ggplot(joint_data_complete, aes(y = subjectivity_d1b, x = volume_ld1b)) +
    geom_point()

###

ggplot(joint_data, aes(x = volume)) +
    geom_histogram()

ggplot(joint_data, aes(x = subjectivity)) +
    geom_histogram()

ggplot(joint_data, aes(x = polarity)) +
    geom_histogram()

###

ggplot(joint_data_complete, aes(y = subjectivity, x = polarity, colour = volume)) +
    geom_point()
ggplot(joint_data_complete, aes(y = subjectivity_d1a, x = polarity_d1a, colour = volume)) +
    geom_point()
ggplot(joint_data_complete, aes(y = subjectivity_d1b, x = polarity_d1b, colour = volume)) +
    geom_point()



fit <- lm(volume ~ subjectivity + polarity + subjectivity_d1a + polarity_d1a, data = joint_data_complete)
summary(fit)

fit <- lm(volume_d1b ~ subjectivity + polarity + subjectivity_d1a + polarity_d1a, data = joint_data_complete)
summary(fit)

fit <- lm(volume_ld1b ~ subjectivity + polarity + subjectivity_d1a + polarity_d1a, data = joint_data_complete)
summary(fit)

fit <- lm(volume ~ poly(polarity, degree = 2), data = joint_data_complete)
summary(fit)

fit <- lm(volume_ld1b ~ poly(polarity, degree = 2), data = joint_data_complete)
summary(fit)

joint_data_complete$polarity_2 <- joint_data_complete$polarity^2
fit <- lm(volume_ld1b ~ polarity + polarity_2, data = joint_data_complete)
summary(fit)

fit <- lm(volume_ld1b ~ polarity, data = joint_data_complete)
summary(fit)

fit <- lm(volume_ld1b ~ subjectivity + polarity + subjectivity * polarity, data = joint_data_complete)
summary(fit)

# scatterplot subjectivity vs volume
