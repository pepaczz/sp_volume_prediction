# load packages
require(ggplot2)
require(dplyr)
require(tidyquant)
require(stargazer)
require(lubridate)
require(QuantTools)
require(reshape2)
require(readr)
require(qqplotr)
require(R.utils)

# source functions
source("R/constants.r")
file.sources = list.files(c("R/model_scripts", 
                            "R/other_scripts", 
                            "R/plots_and_analyses" ), 
                          pattern="*.R$", full.names=TRUE, 
                          ignore.case=TRUE)
sapply(file.sources,source,.GlobalEnv)

# generate exploratory figures
plot_sp_series() # generate plot with SP time series
plot_sp_ema() # generate plot with SP time series with EMA
calibration_data <- calibrate_model_ema() # generate plot for EMA model calibration

# fit models
fit_ema <- model_ema(fit_model = T, ema_order = 5)
fit_nikkei <- model_nikkei(fit_model = T)
fit_nikkei_sq <- model_nikkei_sq(fit_model = T)
fit_oe <- model_other_economic(fit_model = T)
fit_oe_sq <- model_other_economic_sq(fit_model = T)
fit_calendar_hv <- model_calendar(fit_model = T, high_volatility_only = T)
fit_calendar_lv <- model_calendar(fit_model = T, high_volatility_only = F)
fit_sentiment <- model_sentiment(fit_model = T)
fit_sentiment_sq <- model_sentiment_sq(fit_model = T)

# summarize models
summary(fit_ema)
summary(fit_nikkei)
summary(fit_nikkei_sq)
summary(fit_oe)
summary(fit_oe_sq)
summary(fit_calendar_hv)
summary(fit_calendar_lv)
summary(fit_sentiment)
summary(fit_sentiment_sq)

# predict models
model_ref(write_to_file = T, start_date = TEST_START, end_date = TEST_END)
model_ema(fit_model = F, ema_order = 5, start_date = TEST_START, end_date = TEST_END)
model_nikkei(fit_model = F, start_date = TEST_START, end_date = TEST_END)
model_nikkei_sq(fit_model = F, start_date = TEST_START, end_date = TEST_END)
model_other_economic(fit_model = F, start_date = TEST_START, end_date = TEST_END)
model_other_economic_sq(fit_model = F, start_date = TEST_START, end_date = TEST_END)
model_calendar(fit_model = F, high_volatility_only = T, start_date = TEST_START, end_date = TEST_END)
model_calendar(fit_model = F, high_volatility_only = F, start_date = TEST_START, end_date = TEST_END)
model_sentiment(fit_model = F, start_date = TEST_START, end_date = TEST_END)
model_sentiment_sq(fit_model = F, start_date = TEST_START, end_date = TEST_END)

# generate LaTeX outputs
stargazer::stargazer(fit_ema) # generate LaTeX output
stargazer::stargazer(fit_nikkei, fit_nikkei_sq) # generate LaTeX output
stargazer::stargazer(fit_oe, fit_oe_sq)
stargazer::stargazer(fit_calendar_hv, fit_calendar_lv)
stargazer::stargazer(fit_sentiment, fit_sentiment_sq)

# generate list of model files
list_of_models <- c(MODEL_REF,
                    MODEL_CALENDAR_HV,
                    MODEL_CALENDAR_LV,
                    MODEL_NIKKEI,
                    MODEL_NIKKEI_SQ,
                    MODEL_SENTIMENT,
                    MODEL_SENTIMENT_SQ,
                    MODEL_EMA,
                    MODEL_OTHER_ECONOMIC,
                    MODEL_OTHER_ECONOMIC_SQ)

# overal plot of predictions (not used)
gg_data <- merge_model_data(list_of_models, return_gg_format = T) #, filter_year = 2012)
ggplot(gg_data, aes(x = date, y = value, colour = model)) +
    geom_line()

# evaluate models
models_eval_df <- get_evaluation_metrics(list_of_models) %>%
    arrange(-rmse)
stargazer(models_eval_df, summary = F, rownames = F)

# qqplots of residuals
plot_residuals_densities(list_of_models[1:5], fig_file = "residuals_densities_1.png")
plot_residuals_densities(list_of_models[6:10], fig_file = "residuals_densities_2.png")


