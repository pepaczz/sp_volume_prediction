# -*- coding: utf-8 -*-
"""
Created on Sat Jun  2 11:07:52 2018

@author: Josef
"""
import os
TWITTER_SPECIFICATIONS_FOLDER = 'twitter_search_specifications'

def main():
    # Wrapper aroud Get Old Tweets tool
    
    fnm = 'twitter_search_specifications.txt'
    fnm_full = os.path.join(TWITTER_SPECIFICATIONS_FOLDER, fnm)

    # open file
    with open(fnm_full, 'r') as handle:
        
        # iterate over each specification for twitter analysis
        for ind, line in enumerate(handle):
            
            print("file specification " + str(ind + 1))
            print(line)
            # run the twitter data exporter
            os.system('python d:/codes/GetOldTweets-python/Exporter.py %s' % line)

             
if __name__ == '__main__':
	main()
    
