# -*- coding: utf-8 -*-
"""
Created on Fri Jun  1 14:15:07 2018

@author: Josef
"""

from textblob import TextBlob
import pandas as pd
import os



TWITTER_DATA_FLD = os.path.join(os.path.dirname(os.getcwd()),"data/sentiment_data/twitter_data")
DERIVED_SENTIMENT_FLD = os.path.join(os.path.dirname(os.getcwd()),"data/sentiment_data/derived_sentiment")

def derive_twitter_sentiment(twitter_data_fld = TWITTER_DATA_FLD,
                             derived_sentiment_fld = DERIVED_SENTIMENT_FLD):
    # derives twitter sentiment from twitter data
    #
    # args:
    #    twitter_data_fld: folder with input scraped tweets
    #    derived_sentiment_fld: output folder
    
    
    # iterate over all files in tweets folder
    for file_nm in os.listdir(twitter_data_fld):
        
        print("Deriving sentiment for %s" %file_nm)
        
        df = pd.read_csv(os.path.join(twitter_data_fld, file_nm), sep=';')
        
        # iterate over all tweets
        for ind, text in enumerate(df['text']):
        
            # derive the sentiment
            analysis = TextBlob(text)
            df.loc[ind, 'polarity'] = analysis.sentiment[0]
            df.loc[ind, 'subjectivity'] = analysis.sentiment[1]
            
            # convert date
            df.loc[ind, 'date'] = df.loc[ind, 'date'][0:10]
            
        # get only non-zero observations
        df = df.loc[(df.loc[:, 'polarity'] != 0) | (df.loc[:, 'subjectivity'] != 0), :]
        
        # select appropriate columns
        df_2 = df[['date', 'subjectivity', 'polarity']]
        
        # average subjectivity and polarity across dates
        df_2 = df_2.groupby(['date'],  as_index=False).mean()
        
        # export to csv
        df_2.to_csv(os.path.join(derived_sentiment_fld, file_nm), index=False)
        
if __name__ == "__main__":
    derive_twitter_sentiment()