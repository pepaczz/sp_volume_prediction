# qMiners - vstupní úloha #

Autor: Josef Brechler (c)
Datum: 13.6.2018

Toto je technická dokumentace k projektu qMiners - vstupní úloha, jehož cílem je vytvoření predikčního modelu pro objem obchodů indexu S&P500.

***

## Hlavní funkcionalita skriptů

 - preprocessing dat
 - odhad predikčních modelů
 - vytvoření datových a grafických výstupů
 - vytvoření výstupů pro vytvoření dokumentu ve formátu `LaTeX`

***

## Přehled použitých modelovacích technik

Všechny použité modely zařazené do prezentovaných výsledků jsou lineární regrese a liší se vysvětlovanými proměnnými a časovým idexem  u těchto proměnných (tj. jedná se o lineární regresi použitou na časové řady). Následuje seznam modelů se stručným popisem, pro podrobněší popis a formální definici viz přiložený hlavní dokument s výsledky.

 - MODEL_REF - referenční model
 - MODEL_EMA - model využívající exponenciální klouzavý průměr
 - MODEL_CALENDAR_HV - model využívající kalendář eventů, a to pouze eventy s vysokou důležitostí
 - MODEL_CALENDAR_LV - model využívající kalendář eventů, a to eventy s vysokou i nízkou důležitostí
 - MODEL_NIKKEI - model založený na využití neopožděných dat z japonské burzy
 - MODEL_NIKKEI_SQ - model založený na využití neopožděných dat z japonské burzy (proměnná v druhé mocnině)
 - MODEL_SENTIMENT - model využávající data Twitter sentimentu
 - MODEL_SENTIMENT_SQ - model využávající data Twitter sentimentu (proměnná v druhé mocnině)
 - MODEL_OTHER_ECONOMIC - model využívající další ekonomické časové řady 
 - MODEL_OTHER_ECONOMIC_SQ - model využívající další ekonomické časové řady (proměnná v druhé mocnině)

***

## Výstupy

### Datové výstupy

 - složka `data/model_files_r` - obsahuje binární soubory (`.rds`) s modely odhadnutými na trénovacích datech. Tyto odhadnuté modely jsou v pozdějších krocích znovu načteny a je pomocí nich provedena predikce na testovací množině dat za účelem odhadu chybovosti modelů
 - složka `data/models_prediction` - jednotlivé podsložky v této složce obsahují predikované hodnoty na testovací množině dat. Tato predikovaná data mají jednotný formát - jedná se vždy soubor `prediction.csv` se dvěma sloupci `date` a `volume` (ve smyslu predikovaného volume).

### Grafické výstupy

Veškeré grafické výstupy jsou generovány do složky `figures`.

***

## Použité technologie

 - `R`
 - `Python 3.6`
 - `LaTeX`
 
`Python` je použitý pro scraping twitterových dat a následý výpočet odvozeného sentimentu. Veškerý ostatní preprocesing dat, modelování a tvoba výstupů probíhá v jazyce `R` (konkrétně byla použita verze `R version 3.5.0 (2018-04-23)`).

V případě části `R` jsou veškeré package načteny na počátky skriptu `main.r`.

***

## Spuštění hlavního skriptu

Hlavní skript je `R/main.r`. Je vhodné tento skript nespouštět najednou, ale krokovat po řádcích. Předpokladem pro funkčnost skriptu jsou připravená vstupní data, viz níže. Tento skript tedy 

 1. načte veškeré potřebné package
 2. načte veškeré potřebné funkce
 3. vygeneruje některé počáteční grafické výstupy
 4. odhadne všechny modely a (vyjma referenčního modelu) uloží objekty modelů do binárních souborů do složky `data/model_files_r`
 5. zobrazí souhrny odhadlých modelů (`summary`)
 6. vypočte predikce a uloží je do `data/models_prediction`
 7. vygeneruje výstupy pro `LaTeX`
 8. vygeneruje závěrečné grafické a datové výstupy

***

## Příprava a preprocessing dat

### Data S&P500

Příprava této datové řady zahrnuje tyto manuální kroky:

 1. stažení [datové řady S&P500 z finance.yahoo.com](https://finance.yahoo.com/quote/%5EGSPC/history?p=%5EGSPC) v rozsahu alespoň 1.1.2009 až 31.12.2016
 2. Pro přehlednost výsledků je třeba vydělit proměnou `Volume` hodnotou 10^9.
 3. Uložení dat pod názvem `data/in/sp500_data/GSPC.csv`

### Data dalších ekonomických ukazatelů a instrumentů

Jedná se o skupinu dat staženou z webu [www.investing.com](https://www.investing.com). Konkrétně se jedná o tyto časové řady v rozsahu minimálně 1.1.2010 až 31.12.2016:

 - [NIKKEI 225 index](https://www.investing.com/indices/japan-ni225-historical-data) jako soubor `NI225.csv`
 - [10Y US bonds](https://www.investing.com/rates-bonds/u.s.-10-year-bond-yield-historical-data) jako soubor `UST10Y.csv`
 - [3M US bonds](https://www.investing.com/rates-bonds/u.s.-3-month-bond-yield-historical-data) jako soubor `UST3M.csv`
 - [měnový pár USD CHF](https://www.investing.com/currencies/usd-chf-historical-data) jako soubor `USDCHF.csv`
 - [index S&500](https://www.investing.com/indices/us-spx-500-historical-data)  jako soubor `GSPC.csv` - v tomto případě se jedná o stejný instrument, který je dle zadání úlohy třeba stáhnout z finance.yahoo.com. Z důvodu jednotného načtní společně s otatními daty je třeba sáhnout tato data duplicitně i ve formátu z webu investing.com.
 
Složkou pro tato data je `data\in\other_economical_data\`

### Ekonomický kalendář

Jedná se o data stažená z [www.fxstreet.com](https://www.fxstreet.com/economic-calendar) v rozsahu minimálně 1.1.2010 až 31.12.2016. Webové rozhraní dovoluje stahovat data po 2 měsících. Cílovou složku pro tato data je `data/in/economic_calendar_data`, data mohou mít libovlný název (skript uploaduje všechny soubory) a příponu `.csv`.

### Twitter sentiment

Příprava těchto dat probíhá ve dvou krocích:

**1. Scraping dat z Twitteru**

Standardní API dovoluje stahovat pouze 2 týdny historie dat. Toto omezení lze však obejít pomocí nástroje [Get Old Tweets](https://github.com/Jefferson-Henrique/GetOldTweets-python). Data tak lze scrapovat v těchto krocích:

1. Stažení [nástroje Get Old Tweets (GOT) z GitHub repository](https://github.com/Jefferson-Henrique/GetOldTweets-python). 
2. Vytvoření souboru se specifikací parametrů, se kterými se GOT volá. Jedná se o soubor `python/twitter_search_specifications/twitter_search_specifications.txt`.
3. Spuštění skriptu `python/scrape_twitter_data.py`, který slouží jako wrapper okolo nástroje GOT a ukládá scrapovaná data do složky `data/sentiment_data/twitter_data`. Před spuštěním je třeba upravit cestu uvnitř skriptu k nástroji GOT.

**2. Odvození sentimentu tweetů**

1. Sentiment je vypočten spuštěním skriptu `python/derive_twitter_sentiment.py`, který výsledky ukládá do složky `data/sentiment_data/derived_sentiment`. V některých případech je třeba manuální úprava stažených Twitter dat např. z důvodů nadbytečných znaků sloužících jako oddělovače sloupců.